# API Specification

This API allows you to reserve a book.

The API is available at `https://simple-books-api.glitch.me`

1. [Status](https://gitlab.com/training299/private/api-testing-with-postman-workshop/-/blob/main/documents/api-specification.md#api-name-status)

2. [Registration](https://gitlab.com/training299/private/api-testing-with-postman-workshop/-/blob/main/documents/api-specification.md#api-name-registration)

3. [List of books](https://gitlab.com/training299/private/api-testing-with-postman-workshop/-/blob/main/documents/api-specification.md#api-name-list-of-books)

4. [Get a single book](https://gitlab.com/training299/private/api-testing-with-postman-workshop/-/blob/main/documents/api-specification.md#api-name-get-a-single-book)

5. [Submit an order](https://gitlab.com/training299/private/api-testing-with-postman-workshop/-/blob/main/documents/api-specification.md#api-name-submit-an-order)

6. [Get an order](https://gitlab.com/training299/private/api-testing-with-postman-workshop/-/blob/main/documents/api-specification.md#api-name-get-an-order)

## API name: Status
### Description
Check this API available

#### Endpoint: /status
#### Method: GET

##### Request headers
```json
{
    "Content-Type": "application/json"
}
```
##### Request body


##### Response headers
```json
{
    "Content-Type": "application/json"
}
```
##### Status code: 200
##### Response body
```json
{
    "status": "OK"
}
```

| **Name**  | **Type**     | **Description**  |
|---------|----------|--------|
| status | string | API available |

## API name: Registration
### Description
To submit or view an order, you need to register your API client.

#### Endpoint: /api-clients/
#### Method: POST
##### Request headers
```json
{
    "Content-Type": "application/json"
}
```
##### Request body
```json
{
    "clientName": "suntisuk",
    "clientEmail": "suntisuk.dev@gmail.com"
}
```

| **Name**  | **Type**     | **Description**  |
|---------|----------|--------|
| client_name | string | Full name |
| client_email | string | Email name |

##### Response headers
```json
{
    "Content-Type": "application/json"
}
```
##### Status code: 201
##### Response body
```json
{
    "accessToken": "574ce47f3e6d4079c61fe98fe5d09ec1045437d0222cd40863114806139e0621"
}
```

| **Name**  | **Type**     | **Description**  |
|---------|----------|--------|
|  |  |  |

## API name: List of books
### Description
Returns a list of books.

Optional query parameters:

- type: fiction or non-fiction
- limit: a number between 1 and 20.

#### Endpoint: /books
#### Method: GET
##### Request headers
```json
{
    "Content-Type": "application/json"
}
```
##### Request body


##### Response headers
```json
{
    "Content-Type": "application/json"
}
```
##### Status code: 200
##### Response body
```json
[
    {
        "id": 1,
        "name": "The Russian",
        "type": "fiction",
        "available": true
    },
    {
        "id": 2,
        "name": "Just as I Am",
        "type": "non-fiction",
        "available": false
    },
    {
        "id": 3,
        "name": "The Vanishing Half",
        "type": "fiction",
        "available": true
    },
    {
        "id": 4,
        "name": "The Midnight Library",
        "type": "fiction",
        "available": true
    },
    {
        "id": 5,
        "name": "Untamed",
        "type": "non-fiction",
        "available": true
    },
    {
        "id": 6,
        "name": "Viscount Who Loved Me",
        "type": "fiction",
        "available": true
    }
]
```

| **Name**  | **Type**     | **Description**  |
|---------|----------|--------|
| id | integer | Book ID |
| name | string | book name |
| type | string | kind of book |
| available | boolean | In store |

## API name: Get a single book
### Description
Retrieve detailed information about a book.

#### Endpoint: /books/:bookId

Ex: /books/1

#### Method: GET
##### Request headers
```json
{
    "Content-Type": "application/json"
}
```
##### Request body


##### Response headers
```json
{
    "Content-Type": "application/json"
}
```
##### Status code: 200
##### Response body
```json
{
    "id": 1,
    "name": "The Russian",
    "author": "James Patterson and James O. Born",
    "isbn": "1780899475",
    "type": "fiction",
    "price": 12.98,
    "current-stock": 12,
    "available": true
}
```

| **Name**  | **Type**     | **Description**  |
|---------|----------|--------|
| id | integer | Book ID |
| name | string | Book name |
| author | string | Author name |
| isbn | string | ISBN No. |
| type | string | Kind of book |
| price | integer | Price per book |
| current_stock | integer | Quantity |
| available | boolean | In store |

## API name: Submit an order
### Description
Allows you to submit a new order. Requires authentication.

The request body needs to be in JSON format and include the following properties:

 - `bookId` - Integer - Required
 - `customerName` - String - Required

#### Endpoint: /orders/
#### Method: POST
##### Request headers

Ex:

Authorization: "Bearer <YOUR TOKEN>"

Authorization: "Bearer 574ce47f3e6d4079c61fe98fe5d09ec1045437d0222cd40863114806139e0621"

```json
{
    "Content-Type": "application/json",
    "Authorization": "Bearer <YOUR TOKEN>"
}
```
##### Request body
```json
{
    "bookId": 1,
    "customerName": "Suntisuk Khemphed"
}
```

| **Name**  | **Type**     | **Description**  |
|---------|----------|--------|
| book_id | integer | Book ID |
| customer_name | string | Customer Full name |

##### Response headers
```json
{
    "Content-Type": "application/json"
}
```
##### Status code: 201
##### Response body
```json
{
    "created": true,
    "orderId": "VWHI0BfAxaOeaByfi3T3K"
}
```

| **Name**  | **Type**     | **Description**  |
|---------|----------|--------|
| created | boolean | New order |
| order_id | string | Order ID |

## API name: Get an order
### Description
Allows you to view an existing order. Requires authentication.

#### Endpoint: /orders/:orderId

Ex:

/orders/:orderId

/orders/VWHI0BfAxaOeaByfi3T3K

#### Method: GET
##### Request headers

Ex:

Authorization: "Bearer <YOUR TOKEN>"

Authorization: "Bearer 574ce47f3e6d4079c61fe98fe5d09ec1045437d0222cd40863114806139e0621"

```json
{
    "Content-Type": "application/json",
    "Authorization": "Bearer <YOUR TOKEN>"
}
```
##### Request body
##### Response headers
```json
{
    "Content-Type": "application/json"
}
```
##### Status code: 200
##### Response body
```json
{
    "id": "VWHI0BfAxaOeaByfi3T3K",
    "bookId": 1,
    "customerName": "Suntisuk Khemphed",
    "createdBy": "7fbe3b251e56223e9af34ae4246f32a1bc8359b107a418fc1b019e3d2dab7709",
    "quantity": 1,
    "timestamp": 1643698940471
}
```

| **Name**  | **Type**     | **Description**  |
|---------|----------|--------|
| order_id | string | Order ID |
| book_id | integer | Book ID |
| customer_name | string | Customer Full name |
| quantity | integer | Quantity per item order |
| created_by | string |  |
| timestamp | timestamp | Date order |

## API name: Get all orders
### Description
Allows you to view all orders. Requires authentication.

#### Endpoint: /orders

#### Method: GET
##### Request headers

Ex:

Authorization: "Bearer <YOUR TOKEN>"

Authorization: "Bearer 574ce47f3e6d4079c61fe98fe5d09ec1045437d0222cd40863114806139e0621"

```json
{
    "Content-Type": "application/json",
    "Authorization": "Bearer <YOUR TOKEN>"
}
```
##### Request body
##### Response headers
```json
{
    "Content-Type": "application/json"
}
```
##### Status code: 200
##### Response body
```json
[
    {
        "id": "0iEDVaEPoBQBDu6OUlY0n",
        "bookId": 1,
        "customerName": "Suntisuk Khemphed",
        "createdBy": "86cffe07b600db657df1b191f9606e6707705daadbdcb3324aa39985797b7747",
        "quantity": 1,
        "timestamp": 1643701276480
    }
]
```

| **Name**  | **Type**     | **Description**  |
|---------|----------|--------|
| order_id | string | Order ID |
| book_id | integer | Book ID |
| customer_name | string | Customer Full name |
| quantity | integer | Quantity per item order |
| created_by | string |  |
| timestamp | timestamp | Date order |


